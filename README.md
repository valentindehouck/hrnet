# HRnet

La mission principale est de refondre une application en remplaçant jQuery par React

Repository contenant le code du projet d'origine : https://github.com/OpenClassrooms-Student-Center/P12_Front-end

Une librairie React a été crée afin de remplacer
le [plugin jQuery DataTables](https://datatables.net/) : https://www.npmjs.com/package/react-plugin-table

## Environnement technique du projet

- Vite
- TailwindCSS
- React
- Typescript
- ESLint

## Dépendances

- react-hook-form
- react-plugin-table
- react-router-dom
- yup

## Lancement du projet en local

Clonez le repository puis exécutez les commandes suivantes dans le dossier du projet :

```
npm install
npm run dev
```

Un fichier de données fictives a été créé dans le dossier ``src/data`` nommé ``mock_data.ts`` et peut être importé pour
les utiliser dans le tableau