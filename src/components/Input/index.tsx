import {FieldError} from 'react-hook-form'
import {forwardRef, HTMLProps} from 'react'

interface InputProps extends HTMLProps<HTMLInputElement> {
    label: string
    error: FieldError | undefined
}

const Input = forwardRef<HTMLInputElement, InputProps>(({label, error, ...props}, ref) => {
    return (
        <div>
            <label className="block text-gray-700 font-bold mb-2" htmlFor={props.id}>
                {label}
            </label>
            <input className="border border-gray-400 rounded py-2 px-3 w-full aria-[invalid=true]:border-red-500 aria-[invalid=true]:bg-red-50 aria-[invalid=true]:text-red-900 aria-[invalid=true]:placeholder-red-700"
                   aria-invalid={error ? "true" : "false"} aria-required="true" ref={ref} {...props} />
            {error && <span role="alert" className="text-red-700 text-sm">{error?.message}</span>}
        </div>
    )
})

export default Input