import {FieldError} from 'react-hook-form'
import {forwardRef, HTMLProps} from 'react'

interface SelectProps extends HTMLProps<HTMLSelectElement> {
    label: string
    error: FieldError | undefined
}

const Select = forwardRef<HTMLSelectElement, SelectProps>(({label, children, error, ...props}, ref) => {
    return (
        <div>
            <label className="block text-gray-700 font-medium mb-2" htmlFor={props.id}>
                {label}
            </label>
            <select className="border border-gray-400 rounded w-full py-2 px-3 bg-transparent" ref={ref} {...props}>
                {children}
            </select>
            {error && <span role="alert" className="text-red-700 text-sm">{error?.message}</span>}
        </div>
    )
})

export default Select