import {NavLink} from 'react-router-dom'

function Header() {
    return (
        <header className="sticky gap-5 flex-wrap top-0 flex justify-between px-10 py-5 align-middle z-10 bg-white">
            <h1 className="text-center">HRnet</h1>
            <nav className="flex flex-wrap gap-5">
                <NavLink to='/' className={({isActive}) => isActive ? 'underline' : 'text-gray-700'}>Create Employee</NavLink>
                <NavLink to='/employees' className={({isActive}) => isActive ? 'underline' : 'text-gray-700'}>View Current Employees</NavLink>
            </nav>
        </header>
    )
}

export default Header