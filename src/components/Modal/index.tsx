import {ReactNode} from 'react'

function Modal({children, isOpen}: { children: ReactNode, isOpen: boolean }) {

    return (
        <>
            <dialog open={isOpen} className={`${isOpen ? "fixed" : "hidden"} z-20 fixed inset-0 overflow-y-auto flex flex-col items-center justify-center rounded-lg p-10 gap-2`}>
                {children}
            </dialog>
            <div className={`${isOpen ? "fixed" : "hidden"} z-10 inset-0 bg-gray-500 bg-opacity-75 transition-opacity`}></div>
        </>
    )
}

export default Modal