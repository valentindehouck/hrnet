import {useForm} from 'react-hook-form'
import {yupResolver} from '@hookform/resolvers/yup'
import * as yup from 'yup'
import listState from '../../data/states_list.json'
import {useState} from 'react'
import {Modal, Input, Select} from '../../components'
import {Link} from 'react-router-dom'
import {Employee} from '../../types'
import {useEmployees} from '../../services/router.tsx'

const schema = yup.object({
    firstName: yup.string().required('First name is a required field'),
    lastName: yup.string().required('Last Name is a required field'),
    dateBirth: yup.date().typeError('Enter a valid date').required('Date of Birth is a required field').max(new Date().toISOString().substring(0, 10), "Date cannot be a future date"),
    startDate: yup.date().typeError('Enter a valid date').required('Start Date is a required field'),
    street: yup.string().required('Street is a required field'),
    city: yup.string().required('City is a required field'),
    state: yup.string().required('State is a required field'),
    zipCode: yup.number().typeError('Enter a number between 5 and 10 digits').required('Zipcode is a required field').positive(),
    department: yup.string().required('Department is a required field')
})

function Home() {
    const { setEmployees } = useEmployees()
    const [showModal, setShowModal] = useState<boolean>(false)

    const {
        register,
        handleSubmit,
        formState: {errors},
    } = useForm({resolver: yupResolver(schema)})

    const onSubmit = (data: yup.InferType<typeof schema>) => {
        setShowModal(true)
        const newEmployee : Employee = {
            id: self.crypto.randomUUID(),
            ...data
        }
        setEmployees((prevEmployees) => [...(prevEmployees ?? []), newEmployee])
    }

    return (
        <>
            <form className="flex flex-col gap-4" onSubmit={handleSubmit(onSubmit)}>
                <Input type="text" label="First Name" id="firstname" placeholder="John"
                       error={errors.firstName} {...register("firstName")} />
                <Input type="text" label="Last Name" id="lastName" placeholder="Doe"
                       error={errors.lastName} {...register("lastName")} />
                <Input type="date" label="Date of Birth" id="dateBirth"
                       error={errors.dateBirth} {...register("dateBirth")} />
                <Input type="date" label="Start Date" id="startDate"
                       error={errors.startDate} {...register("startDate")} />
                <fieldset className="flex flex-col gap-4 rounded-lg border border-gray-200 p-3">
                    <legend className="text-gray-700">Address</legend>
                    <Input type="text" label="Street" id="street" placeholder="1610 Main Street"
                           error={errors.street} {...register("street")} />
                    <Input type="text" label="City" id="city" placeholder="New York"
                           error={errors.city} {...register("city")} />
                    <Select id="state" label="State" error={errors.state} {...register("state")}>
                        {listState.map((state) => {
                            return (
                                <option key={state.abbreviation} value={state.abbreviation}>{state.name}</option>
                            )
                        })
                        }
                    </Select>
                    <Input type="number" label="Zip Code" id="zipCode" placeholder="10001"
                           error={errors.zipCode} {...register("zipCode")} />
                </fieldset>
                <Select id="department" label="Department" error={errors.department} {...register("department")}>
                    <option value="Sales">Sales</option>
                    <option value="Marketing">Marketing</option>
                    <option value="Engineering">Engineering</option>
                    <option value="Human Resources">Human Resources</option>
                    <option value="Legal">Legal</option>
                </Select>
                <button type="submit"
                        className="focus:outline-none text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 rounded-lg py-2.5">Save
                </button>
            </form>
            <Modal isOpen={showModal}>
                <svg xmlns="http://www.w3.org/2000/svg" width="50px" height="50px" viewBox="0 0 50 50"
                     version="1.1">
                    <path stroke="none" fill="rgb(29.803922%,68.627451%,31.372549%)"
                          d="M 3.125 25 C 3.125 37.082031 12.917969 46.875 25 46.875 C 37.082031 46.875 46.875 37.082031 46.875 25 C 46.875 12.917969 37.082031 3.125 25 3.125 C 12.917969 3.125 3.125 12.917969 3.125 25 Z M 3.125 25 "/>
                    <path stroke="none" fill="rgb(80%,100%,56.470588%)"
                          d="M 36.035156 15.183594 L 21.875 29.394531 L 16.066406 23.535156 L 13.136719 26.464844 L 21.875 35.207031 L 38.964844 18.113281 Z M 36.035156 15.183594 "/>
                </svg>
                <h3 className="font-semibold">Form submission</h3>
                <p>Employee Created!</p>
                <Link to="/employees"
                      className="focus:outline-none text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 rounded-lg text-sm px-5 py-2.5 mt-5">
                    See list of employees
                </Link>
            </Modal>
        </>
    )
}

export default Home