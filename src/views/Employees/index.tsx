import {useOutletContext} from 'react-router-dom'
import {EmployeesList} from '../../types'
import {Table} from 'react-plugin-table'
// import data from '../../data/mock_data.ts'

function Employees() {
    const {employees} = useOutletContext<{ employees: EmployeesList | null }>()

    const columns = [
        {
            label: 'First Name',
            cell: 'firstName',
            isSortable: true,
        },
        {
            label: 'Last Name',
            cell: 'lastName',
            isSortable: true,
        },
        {
            label: 'Start Date',
            cell: 'startDate',
            isSortable: true,

        },
        {
            label: 'Department',
            cell: 'department',
            isSortable: true,
        },
        {
            label: 'Date of Birth',
            cell: 'dateBirth',
            isSortable: true,
        },
        {
            label: 'Street',
            cell: 'street',
            isSortable: true,
        },
        {
            label: 'City',
            cell: 'city',
            isSortable: true,
        },
        {
            label: 'State',
            cell: 'state',
            isSortable: true
        },
        {
            label: 'Zip Code',
            cell: 'zipCode',
            isSortable: true
        }
    ];

    return (
        <Table columns={columns} label="Current Employees" isFiltered={true} rows={employees}/>
    )
}

export default Employees
