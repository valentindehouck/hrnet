import {createBrowserRouter, Outlet, useOutletContext} from 'react-router-dom'
import {Employees, Home} from '../views'
import {useState, Dispatch, SetStateAction} from 'react'
import {EmployeesList} from '../types'
import {Header} from '../components'

type ContextType = {
    employees: EmployeesList | null
    setEmployees: Dispatch<SetStateAction<EmployeesList>>
}

function Layout() {
    const [employees, setEmployees] = useState<EmployeesList | null>(null)

    return (
        <>
            <Header />
            <main className="px-10 py-5">
                <Outlet context={{employees, setEmployees}} />
            </main>
        </>
    )
}

const router = createBrowserRouter([
    {
        element: <Layout/>,
        children: [
            {
                path: "/",
                element: <Home/>
            },
            {
                path: "/employees",
                element: <Employees/>
            }
        ]
    }
])

export function useEmployees() {
    return useOutletContext<ContextType>()
}

export default router