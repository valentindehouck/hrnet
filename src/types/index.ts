export type Employee = {
    id: string
    firstName: string
    lastName: string
    dateBirth: Date
    startDate: Date
    street: string
    city: string
    state: string
    zipCode: number
    department: string
}

export type EmployeesList = Employee[]
